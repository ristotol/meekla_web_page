﻿<%@ Page Language="C#" AutoEventWireup="True" EnableEventValidation="false" CodeBehind="AboutUs.aspx.cs" Inherits="Lando.AboutUs" Async="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Meekla is built to make owning your things easier, smarter, more fun. You simply store some info of your things in the app, and your life gets easier." />
    <meta name="author" content="Meekla" />
    <meta property="og:description" content="Meekla is built to make owning your things easier, smarter, more fun. You simply store some info of your things in the app, and your life gets easier." />
    <meta property="og:locale" content="en-us" />
    <meta property="og:site_name" content="Meekla" />
    <meta property="og:title" content="Meekla" />
    <meta property="og:image" content="https://www.meekla.com/img/ogimage.jpg" />
    <meta property="fb:app_id" content="1338603682821001" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <title>Meekla</title>
    <link href="/Content/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/Content/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/Content/vegas.min.css" rel="stylesheet" type="text/css" />
    <link href="/Content/animated.css" rel="stylesheet" type="text/css" />
    <link href="/Content/OwlCarousel/owl.carousel.min.css" rel="stylesheet" type="text/css" />
    <link href="/Content/OwlCarousel/owl.theme.default.min.css" rel="stylesheet" type="text/css" />
    <link href="/Content/style1.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400italic,700italic,400,700" rel="stylesheet" type="text/css" />
    <script src="/scripts/jquery-3.1.1.min.js" type="text/javascript"></script>
    <script src="/Scripts/bootstrap.min.js" type="text/javascript"></script>

</head>
<body>
    <div class="banner1"></div>
    <div class="container">
        <div class="wrapper1">
            <div class="header" style="float: left;">
                <h2>Why we started building Meekla and what is it?</h2>
                <p>You probably own a thing or two. Computers, phones, home appliances, furniture...</p>
                <p>Ari and Risto had been working together for almost 15 years and during that time more than once conversations arose around "what would be a killer app" and although there were a great many ideas, one seemed to comprise a lot of them.</p>
                <p>Issues such as Risto not being able to remember when he should change the air filters of his Roomba robot vacuum cleaner and hoping for an app or a solution that would remind him at the right times, or Ari hoping to remember where he put that one tool... Is it in the shed, at his parents' house, garage, toolshed?</p>
                <p>Or simply trying to find that receipt to check if that coffee machine was still under warranty or not.</p>
                <p>Is this sofa cleanable or not? How should I do it?</p>
                <p>I'd need a drill right now, but do I go and buy a new one or would someone have one to lend?</p>
                <p>Both Risto and Ari are technically savvy and spend some of their spare time digging through the internet for more information about whatever is interesting to them at the moment. Search engines have been able to provide a lot of material including reviews and information about new features, but no single service seems to comprise this information in a clear way.</p>
                <p>All these separate issues already have various single point solutions, but no one seemed to offer an app to provide it all.</p>
                <p>Any inventory app out there was built just for creating an inventory, which to be honest, is a somewhat boring task.</p>
                <p>Any reminder solution out there required you to know what and when you needed reminders for.</p>
                <p>You could search the internet for manuals, but even then, it might actually be quite hard to know what was the exact model of that coffee machine you bought?</p>
                <p>The idea of a centralized database, where people would record what they own, share information with another and as a cherry on top: some intelligence and automation built into the system to do some of the tasks for people.</p>
                <p>Each physical thing would get their own identity and their life would be recorded on the internet.</p>
                <p>Once there's enough users and information, the ability to create services like user to user renting, selling, loaning suddenly become available. Planning a family trip and worried about all the stuff you need to pack? Perhaps you could check if someone at your destination has some extra equipment available for your baby to make the trip a little more comfortable.</p>
                <p>In September of 2016 the time was right and these ideas were put into a concept through many discussions, some of them very late at night or early in the morning. The result of these discussions was Meekla: the name for both the company and the app to be created. Ari came up with that name with a simple concept of "it shouldn't mean anything until we give it meaning."</p>
                <p>After some book and study reading, watching some online videos, first surveys and user interviews around the concept were held in November and based on those results the first version of the app began to form.</p>
                <p>On the 6th of December, first Meeklers were added to the service as beta testers, and the story of Meekla began...</p>
                <h2>The people behind Meekla:</h2>
                <h3>Founder, Ari Hietam&auml;ki</h3>
                <p><a href="https://fi.linkedin.com/in/ari-hietam&auml;ki-33a5935">https://fi.linkedin.com/in/ari-hietam&auml;ki-33a5935</a></p>
                <p>Ari is the main founder of Meekla and an experienced entrepreneur with a strong technical background.&nbsp; Operating as a CEO of Nevtor Oy, Ari was able to grow the IT service business from a few consultants to multimillion euro business.</p>
                <p>The same drive and passion that helped Nevtor Oy grow has re-ignited.</p>
                <p>&nbsp;</p>
                <p>"We will create something exceptional, something that will leave a mark. We're using all the latest technology out there, as we're not encumbered by old systems or processes. I hope we can ignite and share the same passion that we have and feel in our users and our future employees. Passion creates purpose."</p>
                <h3>Co-Founder, Risto Tolonen</h3>
                <p><a href="https://www.linkedin.com/in/ristotolonen">https://www.linkedin.com/in/ristotolonen</a></p>
                <p>Risto is the co-founder of Meekla and worked as an employee at Nevtor Oy from the beginning.</p>
                <p>After working as a technical consultant for several years, Risto took over as a director of consulting, moving on to work as a director of sales and marketing, and finally as the director of outsourcing services, working as Ari's right (or left) hand where most needed.</p>
                <p>Customer satisfaction was always high partially due to Risto's efforts and it was at 4.8/5.0 in August of 2016. Risto is still a little bit bothered that they didn't get to 5/5 during his time.</p>
                <p>&nbsp;</p>
                <p>"We're creating something that didn't exist before and we're moving fast. We use our own solution daily and interact with our users in the community. The change from selling to businesses in one country to working with users all around the world is exhilarating. This is a long road so although we are sprinting at first, we have to keep reminding ourselves that this is a marathon. Not all of our visions can be made into reality in one day, but parts of them can and are."</p>
                <p>&nbsp;</p>
            </div>
        </div>
    </div>
    <footer>
        <div class="container">
            <div class="row footer-widgets">
                <div class="col-md-3 col-xs-12">
                    <div class="footer-widget mail-subscribe-widget social-widget wow zoomIn animated" data-wow-offset="10" data-wow-delay="0.5s">
                        <h4>Follow Us<span class="head-line"></span></h4>
                        <ul class="social-icons">
                            <li><a class="facebook" href="https://www.facebook.com/Meekla-112837315830999/"><i class="fa fa-facebook"></i></a></li>
                            <li><a class="twitter" href="https://twitter.com/meek_la"><i class="fa fa-twitter"></i></a></li>
                            <li><a class="linkedin" href="https://www.linkedin.com/company/meekla"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
                <%--                    <div class="col-md-3 col-xs-12">
                        <div class="footer-widget twitter-widget wow zoomIn animated" data-wow-offset="10" data-wow-delay="0.5s">
                            <h4>Twitter Feed<span class="head-line"></span></h4>
                            <ul>
                                <li>
                                    <p><a href="#">@Meek.la </a>Where your's physical items are</p>
                                    <span>28 June 2016</span>
                                </li>
                                <li>
                                    <p><a href="#">@Meek.la </a>How to use Beacons for houses #IoT #SmartHouse</p>
                                    <span>22 June 2015</span>
                                </li>
                                <li>
                                    <p><a href="#">@Meek.la </a>Wow! did get service request under one hour from our cust</p>
                                    <span>18 June 2016</span>
                                </li>
                            </ul>
                        </div>
                    </div>--%>
                <div class="col-md-3 col-xs-12">
                    <div class="footer-widget flickr-widget wow zoomIn animated" data-wow-offset="10" data-wow-delay="0.5s">
                        <h4>Our Sites<span class="head-line"></span></h4>
                        <ul>
                            <li><a href="/">Home</a></li>
                            <li><a href="/AboutUs.aspx">About Meekla</a></li>
                            <li><a href="/Privacy.aspx">Privacy policy</a></li>
                            <li><a href="/">Signup!</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-xs-12">
                    <div class="footer-widget contact-widget wow zoomIn animated" data-wow-offset="10" data-wow-delay="0.5s">
                        <h4>Meekla<span class="head-line"></span></h4>
                        <p>The best app for your things! Contact us with any questions, ideas, etc.</p>
                        <ul>
                            <li><span>Email:</span> info@meekla.com</li>
                            <li><span>Website:</span> www.meekla.com</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="copyright-section wow zoomIn animated" data-wow-offset="10" data-wow-delay="0.5s">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <p>&copy; 2016 Meekla - All Rights Reserved <a href="#">Meekla.com</a></p>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <ul class="footer-nav">
                            <li><a href="#">Sitemap</a></li>
                            <li><a href="/Privacy.aspx">Privacy Policy</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</body>
</html>
