﻿
jQuery(function ($) {

    'use strict';

    /*==============================================================*/
    // owlCarousel
    /*==============================================================*/
    $("#features-slider").owlCarousel(
        {
            center: true,
            loop: true,
            autoPlayTimeout: 3000, //Set AutoPlay to 3 seconds	 
            responsive: {
                0: {
                    items: 1
                },
                480: {
                    items: 2
                },
                768: {
                    items: 3
                }

            },
            pagination: true,
            nav: true,
            autoplay: true,
            autoplayHoverPause: true,
            navText: [
              "<i class='fa fa-angle-left '></i>",
              "<i class='fa fa-angle-right'></i>"
            ],
        });
    $("#future-slider").owlCarousel(
    {
        center: true,
        loop: true,
        autoPlayTimeout: 3000, //Set AutoPlay to 3 seconds	 
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 2
            },
            768: {
                items: 3
            }

        },
        pagination: true,
        nav: true,
        autoplay: true,
        autoplayHoverPause: true,
        navText: [
          "<i class='fa fa-angle-left '></i>",
          "<i class='fa fa-angle-right'></i>"
        ],
    });
    $("#screenshot-slider").owlCarousel({
        autoPlay: 3000, //Set AutoPlay to 3 seconds	 
        center: true,
        loop: true,
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 2
            },
            768: {
                items: 3
            }
        },
        nav: true,
        autoplay: true,
        autoplayHoverPause: true,
        pagination: true,
        navText: [
  "<i class='fa fa-angle-left '></i>",
  "<i class='fa fa-angle-right'></i>"
        ],
    });

});


/*==============================================================*/
//Mobile Toggle Control
/*==============================================================*/
$(function () {
    var navMain = $("#navigation");
    navMain.on("click", "a", null, function () {
        navMain.collapse('hide');
    });
});

/*==============================================================*/
// Menu add class
/*==============================================================*/
(function () {	
	function menuToggle(){
		var windowWidth = $(window).width();
		if(windowWidth > 767 ){
			$(window).on('scroll', function(){
				if( $(window).scrollTop()>700 ){
					$('.navbar').addClass('main-nav');
				} else {
					$('.navbar').removeClass('main-nav');
				};
				if( $(window).scrollTop()>61 ){
					$('.menu-hide-page #navigation').removeClass('menu-hide');						
				} else {
					$('.menu-hide-page #navigation').addClass('menu-hide');
				}
			});
		}else{
			$('.menu-hide-page #navigation').removeClass('menu-hide');	
			$('.navbar').addClass('main-nav');
		};	
	}

	menuToggle();
}());

$(document).on('click', '.navbar-collapse.in', function (e) {
    if ($(e.target).is('a')) {
        $(this).collapse('hide');
    }
});
/*==============================================================*/
// Menu Scrolling
/*==============================================================*/
$('').onePageNav({
    currentClass: 'active',
    filter: ':not(.exclude)',
});

$(document).ready(function () {
    $('[data-toggle="popover"]').popover({
        html: "true",
        title: "Where to find your Facebook ID? Find help here:",
        content: "<a href='http://findmyfbid.com'>http://findmyfbid.com</a>",
        trigger: "focus hover click",
        placement: "auto",
        delay: { "hide": 4000 }
    });
});
$(document).on('click', function (e) {
    $('[data-toggle="popover"]').each(function () {
        //the 'is' for buttons that trigger popups
        //the 'has' for icons within a button that triggers a popup
        if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
            $(this).popover('hide');
        }
    });
});

