﻿<%@ Page Language="C#" AutoEventWireup="True" EnableEventValidation="false" CodeBehind="Default.aspx.cs" Inherits="Lando.Default" Async="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!-- Google Analytics Content Experiment code -->
    <%--    <script>function utmx_section() { } function utmx() { } (function () {
    var
    k = '133140453-4', d = document, l = d.location, c = d.cookie;
    if (l.search.indexOf('utm_expid=' + k) > 0) return;
    function f(n) {
        if (c) {
            var i = c.indexOf(n + '='); if (i > -1) {
                var j = c.
                indexOf(';', i); return escape(c.substring(i + n.length + 1, j < 0 ? c.
                    length : j))
            }
        }
    } var x = f('__utmx'), xx = f('__utmxx'), h = l.hash; d.write(
    '<sc' + 'ript src="' + 'http' + (l.protocol == 'https:' ? 's://ssl' :
    '://www') + '.google-analytics.com/ga_exp.js?' + 'utmxkey=' + k +
    '&utmx=' + (x ? x : '') + '&utmxx=' + (xx ? xx : '') + '&utmxtime=' + new Date().
    valueOf() + (h ? '&utmxhash=' + escape(h.substr(1)) : '') +
    '" type="text/javascript" charset="utf-8"><\/sc' + 'ript>')
})();
    </script>--%>
    <%--    <script>utmx('url', 'A/B');</script>--%>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Meekla is built to make owning your things easier, smarter, more fun. You simply store some info of your things in the app, and your life gets easier." />
    <meta name="author" content="Meekla" />
    <meta property="og:description" content="Meekla is built to make owning your things easier, smarter, more fun. You simply store some info of your things in the app, and your life gets easier." />
    <meta property="og:locale" content="en-us" />
    <meta property="og:site_name" content="Meekla" />
    <meta property="og:title" content="Meekla" />
    <meta property="og:image" content="https://www.meekla.com/img/ogimage.jpg" />
    <meta property="fb:app_id" content="1338603682821001" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <title>Meekla</title>
    <link href="/Content/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/Content/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/Content/vegas.min.css" rel="stylesheet" type="text/css" />
    <link href="/Content/animated.css" rel="stylesheet" type="text/css" />
    <link href="/Content/OwlCarousel/owl.carousel.min.css" rel="stylesheet" type="text/css" />
    <link href="/Content/OwlCarousel/owl.theme.default.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400italic,700italic,400,700" rel="stylesheet" type="text/css" />
    <link href="/Content/style1.css" rel="stylesheet" type="text/css" />
    <script src="/scripts/jquery-3.1.1.min.js" type="text/javascript"></script>
    <script src="/Scripts/bootstrap.min.js" type="text/javascript"></script>
    <script src="/Scripts/nav.js" type="text/javascript"></script>
    <script src="/Scripts/owl.carousel.min.js" type="text/javascript"></script>
    <script src="/Scripts/scroll-top.js" type="text/javascript"></script>
    <script src="/Scripts/vegas.min.js" type="text/javascript"></script>
    <script src="/Scripts/wow.min.js" type="text/javascript"></script>
    <script src="/Scripts/waypoints.min.js" type="text/javascript"></script>
    <script src="/Scripts/main5.js" type="text/javascript"></script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date(); a = s.createElement(o),
            m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-86975245-1', 'auto');
        ga('send', 'pageview');
    </script>
    <script type="text/javascript">
        var appInsights = window.appInsights || function (config) { function i(config) { t[config] = function () { var i = arguments; t.queue.push(function () { t[config].apply(t, i) }) } } var t = { config: config }, u = document, e = window, o = "script", s = "AuthenticatedUserContext", h = "start", c = "stop", l = "Track", a = l + "Event", v = l + "Page", y = u.createElement(o), r, f; y.src = config.url || "https://az416426.vo.msecnd.net/scripts/a/ai.0.js"; u.getElementsByTagName(o)[0].parentNode.appendChild(y); try { t.cookie = u.cookie } catch (p) { } for (t.queue = [], t.version = "1.0", r = ["Event", "Exception", "Metric", "PageView", "Trace", "Dependency"]; r.length;) i("track" + r.pop()); return i("set" + s), i("clear" + s), i(h + a), i(c + a), i(h + v), i(c + v), i("flush"), config.disableExceptionTracking || (r = "onerror", i("_" + r), f = e[r], e[r] = function (config, i, u, e, o) { var s = f && f(config, i, u, e, o); return s !== !0 && t["_" + r](config, i, u, e, o), s }), t }({ instrumentationKey: "1597db24-d938-4953-9c1c-3cd5723e2281" }); window.appInsights = appInsights; appInsights.trackPageView();
    </script>
    <script type="text/javascript">
        function SignedUp() {
            ga('send', 'event', {
                eventCategory: 'LandingPage',
                eventAction: 'Click',
                eventLabel: 'SignUp'
            });
        };
        function LoggedIn() {
            ga('send', 'event', {
                eventCategory: 'LandingPage',
                eventAction: 'Click',
                eventLabel: 'LogIn'
            });
        };
        //$(window).on('beforeunload', function () {
        //    if (document.getElementById('insertedemail').value === "Yes") {
        //        insertuser();
        //    }
        //});
        //function validemail() {
        //    var user = {};
        //    user.contactemail = document.getElementById('contactemail').value;
        //    if (validateEmail(user.contactemail)) {
        //        $.ajax({
        //            type: "POST",
        //            url: "Default.aspx/CheckEmail",
        //            contentType: "application/json; charset=utf-8",
        //            data: '{user: ' + JSON.stringify(user) + '}',
        //            dataType: "json",
        //            success: function (data) {
        //                if (data.d == "Case 11") {
        //                    alert("Email address already added. If you wish to become an early test user, send us an email at info@meekla.com. Thank you!");
        //                    $('#contactemail').val("");
        //                }
        //                else if (data.d == "Case 12") {
        //                    document.getElementById('insertedemail').value = "Yes";
        //                    $('#registered').modal('show');
        //                }
        //            },
        //            error: function (result) { console.log(result) }
        //        });
        //    }
        //    else {
        //        $('#wrongemail').modal('show');
        //    }
        //};
        //function validateEmail($email) {
        //    var emailReg = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
        //    return emailReg.test($email);
        //};
        //function insertuser() {
        //    var user = {};
        //    ga('send', 'event', {
        //        eventCategory: 'LandingPage',
        //        eventAction: 'Click',
        //        eventLabel: 'EmailRegistration'
        //    });
        //    fbq('track', 'CompleteRegistration', {
        //        value: 0.20,
        //        currency: 'EUR'
        //    });
        //    user.contactemail = document.getElementById('contactemail').value;
        //    user.contactfbid = document.getElementById('contactfbid').value;
        //    clickedyes = document.getElementById('clickedyes').value;
        //    if (user.contactfbid === "" && clickedyes === "Yes")
        //    { user.contactfbid = "Yes" }
        //    else if (user.contactfbid === null && clickedyes === "No")
        //    { user.contactfbid = "" };
        //    console.log(user.contactfbid);
        //    console.log(user.contactemail);
        //    $.ajax({
        //        type: "POST",
        //        url: "Default.aspx/SaveUser",
        //        contentType: "application/json; charset=utf-8",
        //        data: '{user: ' + JSON.stringify(user) + '}',
        //        dataType: "json",
        //        success: function (data) {
        //            if (data.d == "Case 1") {
        //                alert("Facebook ID already entered.");
        //            }
        //            else if (data.d == "Case ok") {
        //                console.log("Ok.");
        //            };
        //        },
        //        error: function (result) { console.log(result) }
        //    });
        //    document.getElementById('contactemail').value = "";
        //    document.getElementById('contactfbid').value = "";
        //    document.getElementById('clickedyes').value = "No";
        //    clickedyes = "No";
        //    document.getElementById('insertedemail').value = "No";
        //};

        //function clickedyep() {
        //    document.getElementById('clickedyes').value = "Yes";
        //};
    </script>
    <!-- Facebook Pixel Code -->
    <!-- Facebook Pixel Code -->
    <%--<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '570253896509178'); // Insert your pixel ID here.
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=570253896509178&ev=PageView&noscript=1"
/></noscript>--%>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div id="navigation">
                <div class="navbar navbar-default navbar-fixed-top" role="banner">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="/" style="padding-top:15px">
                                <img class="img-responsive" src="img/Meekla-small.jpg" alt="Meekla Logo" />
                            </a>
                        </div>
                        <nav id="main-menu" class="collapse navbar-collapse navbar-right">
                            <ul class="nav navbar-nav">
                                <li class="mk-backgroud-focus-hover"><a href="#navigation">Home</a></li>
                                <li class="mk-backgroud-focus-hover"><a href="#features">Features</a></li>
                                <li class="mk-backgroud-focus-hover"><a href="#screenshots">Screenshots</a></li>
                                <li class="mk-backgroud-focus-hover"><a href="#howto">How to get started</a></li>
                                <li class="mk-backgroud-focus-hover"><a href="/blog">Blog</a></li>
                                <li class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Sign in
                                        <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" style="background-color: transparent">
                                        <li class="btn btn-block btn-facebook"><a href="https://app.meekla.com/"></a></>
                                    <span class="fa fa-facebook"></span>Log in
                                        </li>
                                        <li class="btn btn-block btn-facebook"><a href="https://app.meekla.com/newuser/"></a></>
                                    <span class="fa fa-facebook"></span>Sign up
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <header class="home padding-150" style="background: url('/img/background2.jpg'); background-size: cover; background-position: center; margin: 0 auto; padding-top: 100px; padding-bottom: 200px;">
                <div class="home padding-150">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="main-content text-left">
                                    <h1 class="section-title" style="color: beige; text-shadow: 0.5px 0.5px #000000;">THE BEST APP FOR YOUR THINGS</h1>
                                    <h3 style="color: beige; text-shadow: 0.5px 0.5px #000000;">Store receipts, keep an inventory, get manuals, share</h3>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="panel-heading" style="border-radius: 10px; background-color: transparent;">

                                    <a class="btn btn-block btn-social btn-lg btn-facebook" href="https://app.meekla.com/newuser/" style="margin-top: 25px" onclick="Signedup()">
                                        <span class="fa fa-facebook"></span>Sign up with Facebook
                                    </a>
                                    <a class="btn btn-block btn-social btn-lg btn-facebook" href="https://app.meekla.com/" onclick="LoggedIn()">
                                        <span class="fa fa-facebook"></span>Log in
                                    </a>

                                    <%--                                    <h1 class="panel-title" style="color: black; text-align: left">Sign up to become a user</h1>
                                    <input type="text" class="contact-email form-control" placeholder="Email.." id="contactemail" />
                                    <input type="text" style="display: none" id="insertedemail" value="" />
                                    <button type="button" class="btn btn-common" name="Submitreg" onclick="validemail()">Submit</button>--%>
                                    <%--                                    <div id="wrongemail" class="modal fade" role="alert">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Please fill in a proper email address.</h4>
                                            </div>
                                        </div>
                                    </div>--%>
                                    <%--                                    <div id="registered" class="modal fade" style="top: 10%; left: 5%; right: 5%; text-align: left" role="dialog" data-backdrop="static" data-keyboard="false">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" onclick="insertuser()">&times;</button>
                                                <h2 class="modal-title">Thank you for registering!</h2>
                                                <div class="modal-body">
                                                    <h4>Wish to become an early test user?</h4>
                                                    <p style="color: black">Email invitations sent in January.</p>
                                                    <div id="buttons" class="collapse in">
                                                        <input type="text" style="display: none" id="clickedyes" value="No" />
                                                        <button type="button" data-toggle="collapse" data-target="#buttons,#fbtestid" class="collapse in btn btn-primary" id="yesbutton" name="becometest" onclick="clickedyep()">Yes</button>
                                                        <button type="button" data-dismiss="modal" class="collapse in btn btn-primary" id="nobutton" onclick="insertuser()">No</button>
                                                    </div>
                                                    <div id="fbtestid" class="collapse">
                                                        <p>For test users, we need your fbid. You can find it using <a href='http://findmyfbid.com'>http://findmyfbid.com</a>.</p>
                                                        <input type="text" class="contact-fbid form-control" placeholder="Facebook ID.." id="contactfbid" />
                                                        <button type="button" data-dismiss="modal" class="btn btn-common" name="Submitreg" onclick="insertuser()">Submit</button>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </header>

            <div id="features">
                <div class="padding-100" style="padding-bottom: 50px">
                    <div class="container">
                        <div class="row">
                            <h3 class="h1 col-sm-4 blue">Become a smarter owner</h3>
                            <div class="col-sm-8">
                                <%--                              facts: <h4 class="h2">add your things here. share information with other meeklers. get reminders for warranties and any maintenance your things need. let your friends see what you're willing to loan them and get their lists.</h4>
                                antagonize:<h4 class="h2"> i need some help with my things. i never remember to do the tasks the user manual says i should do regularly. i store my receipts in a pile in my closet. would love to ask other people how they do stuff with their things, but i'm not gonna join 20 different forums. maybe this is the place for me.</h4>
                                marketing:--<h4 class="h2">bought something new, something nice? great! snap a quick photo of that receipt and add it here. time to start meeting others who are using it, maybe learn a trick or two. oh, and recycle that user manual, we have a copy of it here for you. didn't buy it yet? you might want to find some reviews here, or ask around.</h4>--%>
                                <h4 class="h2">Meekla is built to make owning your things easier, smarter, more fun. You simply store some info of your things in the app, and your life gets easier. Here's a short list of features that you'll be getting immediately:</h4>
                            </div>
                        </div>
                        <%--                    </div>--%>
                    </div>
                </div>
                <div class="text-center">
                    <div class="container padding-20">
                        <div id="features-slider" class="owl-carousel owl-theme">
                            <div class="item">
                                <img class="img-responsive" src="/img/boxpic1-receipts.jpg" alt="" />
                                <h2>Store receipts</h2>
                                <p>Ink wears out, and you want to find it fast when you need it, perhaps while travelling. Store it here.</p>
                            </div>
                            <div class="item">
                                <img class="img-responsive" src="/img/Things.png" alt="" />
                                <h2>Share pictures</h2>
                                <p>Share, like, ask. Users add photos of their thing anonymously and you can like them or check what that thing actually is.</p>
                            </div>


                            <div class="item">
                                <img class="img-responsive" src="/img/maintenance.jpg" alt="" />
                                <h2>Maintenance reminders</h2>
                                <p>Take care of your things on time. They live longer and it's easier to sell them. Also, you'll remember if warranty is valid or not.</p>
                            </div>
                            <div class="item">
                                <img class="img-responsive" src="/img/art.jpg" alt="" />
                                <h2>Find your things</h2>
                                <p>Do you remember everything you own? Do you remember where they are? Take control.</p>
                            </div>
                            <div class="item">
                                <img class="img-responsive" src="/img/Manuals-boxpic.jpg" alt="" />
                                <h2>User manuals</h2>
                                <p>You'll find your manual here, always available. Just open the app and select your thing, the manual is there.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="screenshots" style="background-color: lightgray">
                <div class="container">
                    <div class="padding-100">
                        <div class="row">
                            <h3 class="h1 col-sm-4 blue">Screenshots</h3>
                            <div class="col-sm-8">
                                <h4 class="h2">The app is to travel with your phone, but it works well on browsers.</h4>
                            </div>
                        </div>
                        <div id="screenshot-slider" class="padding-50 owl-carousel owl-theme">
                            <div class="item">
                                <img class="img-responsive" src="/img/screen10.jpg" alt="" />
                            </div>
                            <div class="item">
                                <img class="img-responsive" src="/img/screen7.jpg" alt="" />
                            </div>
                            <div class="item">
                                <img class="img-responsive" src="/img/screen8.jpg" alt="" />
                            </div>
                            <div class="item">
                                <img class="img-responsive" src="/img/screen9.jpg" alt="" />
                            </div>
                            <div class="item">
                                <img class="img-responsive" src="/img/screen4.png" alt="" />
                            </div>
                            <div class="item">
                                <img class="img-responsive" src="/img/screen5.png" alt="" />
                            </div>
                            <div class="item">
                                <img class="img-responsive" src="/img/screen6.png" alt="" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="howto">
                <div class="container">
                    <div class="description-section padding-100">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <h3 class="h1 col-sm-4 blue">How to get started?</h3>
                                    <div class="col-sm-8">
                                        <h4 class="h2">The first step is the hardest: starting. Do it anyway, and you'll find joy. :)</h4>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="descriptions">
                                    <div class="description">
                                        <i class="fa fa-sign-in" aria-hidden="true"></i>
                                        <p>1. Register: use your familiar Facebook login. More options for logging in are coming in the future.</p>
                                    </div>
                                    <div class="description">
                                        <i class="fa fa-mobile" aria-hidden="true"></i>
                                        <p>2. After registering, start putting in your items: start with a few things you own, like your phone and whatever you've purchased recently.</p>
                                    </div>
                                    <div class="description">
                                        <i class="fa fa-picture-o" aria-hidden="true"></i>
                                        <p>3. Upload a picture of your receipt and the thing, fill in the rest of the data.</p>
                                    </div>
                                    <div class="description">
                                        <i class="fa fa-users" aria-hidden="true"></i>
                                        <p>4. Connect with your friends: if you wish, you can easily share your "loan lists" with the people you trust.</p>
                                    </div>
                                    <div class="description">
                                        <i class="fa fa-fort-awesome" aria-hidden="true"></i>
                                        <p>5. Start your adventure with the other features, like our automated manual search.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 text-center">
                                <img class="img-responsive" src="/img/sidepic3.jpg" alt="" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="future" style="background-color: lightgray">
                <div class="padding-100">
                    <div class="container">
                        <div class="row">
                            <h3 class="h1 col-sm-4 blue">What's coming next</h3>
                            <div class="col-sm-8">
                                <%--                              facts: <h4 class="h2">add your things here. share information with other meeklers. get reminders for warranties and any maintenance your things need. let your friends see what you're willing to loan them and get their lists.</h4>
                                antagonize:<h4 class="h2"> i need some help with my things. i never remember to do the tasks the user manual says i should do regularly. i store my receipts in a pile in my closet. would love to ask other people how they do stuff with their things, but i'm not gonna join 20 different forums. maybe this is the place for me.</h4>
                                marketing:--<h4 class="h2">bought something new, something nice? great! snap a quick photo of that receipt and add it here. time to start meeting others who are using it, maybe learn a trick or two. oh, and recycle that user manual, we have a copy of it here for you. didn't buy it yet? you might want to find some reviews here, or ask around.</h4>--%>
                                <h4 class="h2">Here are a few of the features we'll be building in the coming months, based on what you, our users, prefer:</h4>
                            </div>
                        </div>
                        <%--                    </div>--%>
                    </div>
                </div>
                <div class="text-center">
                    <div class="container padding-20">
                        <div id="future-slider" class="owl-carousel owl-theme">
                            <div class="item">
                                <img class="img-responsive" src="/img/car.jpg" alt="" />
                                <h2>Current value</h2>
                                <p>What is the current value of your things? How much would you get for selling them? We'll find out for you.</p>
                            </div>

                            <div class="item">
                                <img class="img-responsive" src="/img/beach.jpg" alt="" />
                                <h2>Find reviews, tips and tricks fast</h2>
                                <p>Don't spend your time looking for information about whether that new phone is good or not. Find it here and use that time at the beach.</p>
                            </div>
                            <div class="item">
                                <img class="img-responsive" src="/img/fire.jpg" alt="" />
                                <h2>Insurance information export</h2>
                                <p>Fire, like burglars, is an unwanted guest. Have a list ready for the insurance, with a click of a button.</p>
                            </div>
                            <div class="item">
                                <img class="img-responsive" src="/img/kettlebells.jpg" alt="" />
                                <h2>Sell/rent things</h2>
                                <p>Some useless stuff laying around? Sell them or rent them out. One option is a "silent sale." Publish it to your friend list.</p>
                            </div>
                            <div class="item">
                                <img class="img-responsive" src="/img/rarethings.jpg" alt="" />
                                <h2>Follow/fan things</h2>
                                <p>Select what things you're interested in, and get a feed of news, user experiences, etc.</p>
                            </div>
                            <div class="item">
                                <img class="img-responsive" src="/img/events.jpg" alt="" />
                                <h2>Meet people with same things</h2>
                                <p>Looking for frisbee golf company, maybe a bike trip? This is the place to connect.</p>
                            </div>
                            <%--                    <div class="item">
                        <img class="img-responsive" src="/img/events.jpg" alt="" />
                        <h2>Smart upgrades</h2>
                        <p>Select a budget and what you wish to upgrade. The app will find a better version for you and automatically sell your old one.</p>
                    </div>--%>
                        </div>
                    </div>
                </div>
            </div>
            <div class="scroll-top" data-spy="affix" data-offset-top="300"><a href="#header"><i class="fa fa-angle-up"></i></a></div>
        </div>
    </form>
    <footer>
        <div class="container">
            <div class="row footer-widgets">
                <div class="col-md-3 col-xs-12">
                    <div class="footer-widget mail-subscribe-widget social-widget wow zoomIn animated" data-wow-offset="10" data-wow-delay="0.5s">
                        <h4>Follow Us<span class="head-line"></span></h4>
                        <ul class="social-icons">
                            <li><a class="facebook" href="https://www.facebook.com/Meekla-112837315830999/"><i class="fa fa-facebook"></i></a></li>
                            <li><a class="twitter" href="https://twitter.com/meek_la"><i class="fa fa-twitter"></i></a></li>
                            <li><a class="linkedin" href="https://www.linkedin.com/company/meekla"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
                <%--                    <div class="col-md-3 col-xs-12">
                        <div class="footer-widget twitter-widget wow zoomIn animated" data-wow-offset="10" data-wow-delay="0.5s">
                            <h4>Twitter Feed<span class="head-line"></span></h4>
                            <ul>
                                <li>
                                    <p><a href="#">@Meek.la </a>Where your's physical items are</p>
                                    <span>28 June 2016</span>
                                </li>
                                <li>
                                    <p><a href="#">@Meek.la </a>How to use Beacons for houses #IoT #SmartHouse</p>
                                    <span>22 June 2015</span>
                                </li>
                                <li>
                                    <p><a href="#">@Meek.la </a>Wow! did get service request under one hour from our cust</p>
                                    <span>18 June 2016</span>
                                </li>
                            </ul>
                        </div>
                    </div>--%>
                <div class="col-md-3 col-xs-12">
                    <div class="footer-widget flickr-widget wow zoomIn animated" data-wow-offset="10" data-wow-delay="0.5s">
                        <h4>Our Sites<span class="head-line"></span></h4>
                        <ul>
                            <li><a href="/">Home</a></li>
                            <li><a href="/AboutUs.aspx">About Meekla</a></li>
                            <li><a href="/Privacy.aspx">Privacy policy</a></li>
                            <li><a href="/">Signup!</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-xs-12">
                    <div class="footer-widget contact-widget wow zoomIn animated" data-wow-offset="10" data-wow-delay="0.5s">
                        <h4>Meekla<span class="head-line"></span></h4>
                        <p>The best app for your things! Contact us with any questions, ideas, etc.</p>
                        <ul>
                            <li><span>Email:</span> info@meekla.com</li>
                            <li><span>Website:</span> www.meekla.com</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="copyright-section wow zoomIn animated" data-wow-offset="10" data-wow-delay="0.5s">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <p>&copy; 2016 Meekla - All Rights Reserved <a href="#">Meekla.com</a></p>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <ul class="footer-nav">
                            <li><a href="#">Sitemap</a></li>
                            <li><a href="/Privacy.aspx">Privacy Policy</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</body>
</html>

