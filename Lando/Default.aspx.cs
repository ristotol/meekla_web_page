﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using Lando.Models;
using System.Data;
using System.Data.SqlClient;
using System.Web.Services;
using System.Web.Script.Services;

namespace Lando
{
    public partial class Default : System.Web.UI.Page
    {
        [WebMethod]
        [ScriptMethod]
        public static string SaveUser(Users user)
        {
            int userID = 0;
            string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand("Insert_User"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@fbid", user.contactfbid);
                    cmd.Parameters.AddWithValue("@Email", user.contactemail);
                    cmd.Connection = con;
                    con.Open();
                    userID = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();
                }
                string message = string.Empty;
                switch (userID)
                {
                    case -1:
                        message = "Case 1";
                        return message;
                    case -2:
                        message = "Case 2";
                        return message;
                    default:
                        message = "Case ok";
                        return message;
                }


            }
        }
        [WebMethod]
        [ScriptMethod]
        public static string CheckEmail(Users user)
        {
            int userID = 0;
            string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
                //try
            {
                using (SqlCommand cmd = new SqlCommand("Check_Email"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@Email", user.contactemail);
                    cmd.Connection = con;
                    con.Open();
                    userID = Convert.ToInt32(cmd.ExecuteScalar());
                    con.Close();
                }
                string message = string.Empty;
                switch (userID)
                {
                    case -11:
                        message = "Case 11";
                            Console.WriteLine(message);
                        return message;
                    case -12:
                        message = "Case 12";
                            Console.WriteLine(message);
                            return message;
                    default:
                        message = "Case ok";
                            Console.WriteLine(message);
                            return message;
                }


            }
            //catch(Exception ex)
            //{ Console.WriteLine(ex.ToString()); }
            //return "Okay";

        }
    }
}