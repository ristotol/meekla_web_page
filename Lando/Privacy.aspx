﻿<%@ Page Language="C#" AutoEventWireup="True" EnableEventValidation="false" CodeBehind="Privacy.aspx.cs" Inherits="Lando.Privacy" Async="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="Meekla is built to make owning your things easier, smarter, more fun. You simply store some info of your things in the app, and your life gets easier." />
    <meta name="author" content="Meekla" />
    <meta property="og:description" content="Meekla is built to make owning your things easier, smarter, more fun. You simply store some info of your things in the app, and your life gets easier." />
    <meta property="og:locale" content="en-us" />
    <meta property="og:site_name" content="Meekla" />
    <meta property="og:title" content="Meekla" />
    <meta property="og:image" content="https://www.meekla.com/img/ogimage.jpg" />
    <meta property="fb:app_id" content="1338603682821001" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <title>Meekla</title>
    <link href="/Content/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/Content/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/Content/vegas.min.css" rel="stylesheet" type="text/css" />
    <link href="/Content/animated.css" rel="stylesheet" type="text/css" />
    <link href="/Content/OwlCarousel/owl.carousel.min.css" rel="stylesheet" type="text/css" />
    <link href="/Content/OwlCarousel/owl.theme.default.min.css" rel="stylesheet" type="text/css" />
    <link href="/Content/style1.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400italic,700italic,400,700" rel="stylesheet" type="text/css" />
    <script src="/scripts/jquery-3.1.1.min.js" type="text/javascript"></script>
    <script src="/Scripts/bootstrap.min.js" type="text/javascript"></script>

</head>
<body>
    <div class="banner1"></div>
    <div class="container">
        <div class="wrapper1">
            <div class="header" style="float: left;">
                <%--                <h1>UNDER CONSTRUCTION.</h1>--%>
                <p>&nbsp;</p>
                <h3>1. Meekla's Privacy policy</h3>
                <p>Meekla Oy (hereinafter referred to as "Meekla" or "we") is a company providing a service for any private owners of physical things. We enable our users to create, gather and maintain an inventory of their property. After adding their things to the inventory, we provide a wide array of services to make owning things easier, smarter, more social and more fun.</p>
                <p>This Privacy policy is intended to describe what information we gather and how we use that information.</p>
                <p>Your personal information and its security is vital to us and to the existence of the service. We take every step available to us to protect it. We never share your personal data with any 3rd parties without your consent or specific action you've taken to share that data.</p>
                <p>We do provide anonymous data aggregated from a wide user base to 3rd parties. You or your personal information will not be recognizable from this aggregated data.</p>
                <p>We may change this Privacy Policy in our sole discretion. If we do make changes to this policy, we will notify you of the change. The notification will be given, if possible, in advance.</p>
                <p>By using the service, user accepts that the data is stored within EU, but may in addition be stored in USA. Please read Information security to learn more.</p>
                <p>Our goal is to at minimum fulfil any requirements outlined in the European Union's General Data Protection Regulation (GDPR) (Regulation (EU) 2016/679) and within the EU-U.S. Privacy Shield Program.</p>
                <h3>2. Information we collect:</h3>
                <p>To enable us to provide the service, we collect data that you insert into the solution:</p>
                <ol>
                    <li>Information about you we receive from you directly:
                       <ul style="list-style-type: circle; padding-left:10px">
                            <li>Your email address</li>
                            <li>Your first name</li>
                            <li>Your last name</li>
                            <li>Your public profile picture</li>
                            <li>Phone number</li>
                            <li>Contact address</li>
                            <li>Gender</li>
                        </ul>
                    </li>
                    <li>Information about you we receive from your Facebook profile if given permission:
                       <ul style="list-style-type: circle; padding-left:10px">
                            <li>Your email address</li>
                            <li>Your first name</li>
                            <li>Your last name</li>
                            <li>Full names of your friends on Facebook. (Only Friends, who also use Meekla's Services)</li>
                            <li>Link to your public profile picture (it is shown from your Facebook page)</li>
                            <li>Phone number</li>
                            <li>Contact address</li>
                            <li>Gender</li>
                        </ul>
                    </li>
                    <li>Information you insert into the solution about your things, if applicable:
                       <ul style="list-style-type: circle; padding-left:10px">
                            <li>Manufacturer</li>
                            <li>Model</li>
                            <li>Type</li>
                            <li>Serial number</li>
                            <li>Purchase date</li>
                            <li>Where it is purchased from</li>
                            <li>Current location</li>
                            <li>Receipts</li>
                            <li>Pictures</li>
                            <li>Warranty period</li>
                            <li>Purchase price</li>
                        </ul>
                    </li>
                    <li>Pseudonymous Information about behavior using the solution:
                        <ul style="list-style-type: circle; padding-left:10px">
                            <li>The IP address, browser and device used to connect to the service</li>
                            <li>Clicks on the service</li>
                            <li>Posts to the service</li>
                            <li>At what times user uses the service</li>
                        </ul>
                    </li>
                    <li>Operative administrative logging which is not used for any other purposes than problem solving and service development by named service administrators and developers. Logs do NOT include any personal information.&nbsp;</li>
                    </ol>
                    <h3>3. What do we use the collected information for:</h3>
                    <p>We use the collected information to</p>
                  <ul style="list-style-type: circle; padding-left:10px">
                        <li>Provide and improve the service for you</li>
                        <li>Secure the service for you</li>
                        <li>Send service related notices to you</li>
                        <li>Provide customer service</li>
                        <li>Monitor the service for errors and performance issues</li>
                        <li>Personalize the service</li>
                    </ul>
                    <p>The pictures added to the public section will be used in a picture bank of that specific device and model. Take care not to post any personal information in the pictures and we remove any such content to the best of our ability. However, it is user's sole responsibility to not add any pictures that contain personal information, or inappropriate content such as pornography, violence or racism.</p>
                    <h3>4. How we share your information:</h3>
                    <p><strong>The pictures</strong> you post on <strong>Public item pictures</strong> are shown on the front page as thumbnails that describe the manufacturer and model of the object you added the picture to.&nbsp; No other data is shared along with the public picture and the item in the picture cannot be linked to you by Third parties or other users.</p>
                    <p>These pictures are considered public and we may use them for marketing purposes. An example of such marketing purposes may be placing them on our public website or using them in our ad campaigns.</p>
                    <p>If you <strong>post a discussion or take part in a discussion</strong> by commenting, that information will be shared with other users. If you click upvote/downvote or like on an object in our service, that like or upvote will be shown to other users as aggregated data. They will not see a list of users, only the number of clicks.</p>
                    <p>We may share aggregated data with Third parties or in our own publishing as long as no individual user or our user's personal data is recognizable from that data. We may share this data for marketing purposes to ad providers or in various publishing.</p>
                  
                    <p>We may share information with our service providers to enable us to provide the service for you. These service providers will be held to our privacy policy described here.</p>
                
                    <p>We may share information between and among Meekla, and its future subsidiaries and other companies under common control and ownership.</p>
                    <h3>5. How you can manage your data:</h3>
                    <p>Meekla is currently in beta testing and lacks removal of information through direct user action within the service. Users may contact us directly at info[at]meekla.com stating that they wish their data removed or with any other privacy concerns including information about the data stored within the service, to which we will respond in a timely fashion, and at latest within one month from the request's registration date.</p>
                 
                    <p>In the near future, we intend to add features to:</p>
                    <ol>
                        <li>Allow users to deactivate, archive, transfer to another user and remove the things they've added.</li>
                        <li>Remove their personal profile information and end service usage.</li>
                        <li>Export the information they've added concerning their things and personal information.</li>
                    </ol>
            
                    <p>While removing user's data, we may still store statistical data regarding user's objects, but any personal information contained within those items will be removed or anonymized.</p>
                    <p>For example, we might store information that object x was purchased from store y at certain date by someone and use that data in our anonymous aggregated data, but that data would no longer be in any way linked to the user who inserted that data.</p>
                 
                    <p>User may opt out of email communications or notifications.</p>
                    <h3>6. Registry controller and contact information regarding this policy</h3>
                    <p>If you have any further questions or require clarification, please contact us at info[at]meekla.com. Any suggestions for additions are more than welcome!</p>
                    <p>Our email address is info[at]meekla.com.</p>
                    <p>You can also reach us by mail at:</p>
                    <p>Meekla Oy (FI2786891-8)</p>
                    <p>Taimenkuja 7</p>
                    <p>01490 Vantaa</p>
                    <p>Finland</p>
                    <h3>7. Information security</h3>
                    <p>All the data stored is stored in a secure Microsoft Azure datacenter in western Europe. Any personal user data is accessible only by authorized Meekla personnel and service contractors, who are bound under the same terms as Meekla personnel.</p>
                    <p>Access to the data within our service is secured according to common standards using different security technologies, including but not limited to firewalls and encrypted connections.</p>
                    <p>Although we are protecting the data to the best of our ability, no entity is able to provide 100% data security.</p>
                    <p>If we notice any threat to information security, we may deny access to the service to all users at any given time.</p>
                    <p>More information regarding the technical platform underlying our services, please read <a href="https://www.microsoft.com/en-us/trustcenter/CloudServices/Azure">https://www.microsoft.com/en-us/trustcenter/CloudServices/Azure</a></p>
                    <%--<p style="font-size:24px; color:gray; text-align:left;">We are in the process of writing our privacy policy.In the meantime: we take privacy and information security very seriously. We will never hand out your personal information to 3rd parties without your explicit consent (a situation where this may be necessary would be for example a sale between two users or providing a delivery address to a supplier for spare parts.)</p>--%>
            </div>
        </div>
    </div>
    <footer>
        <div class="container">
            <div class="row footer-widgets">
                <div class="col-md-3 col-xs-12">
                    <div class="footer-widget mail-subscribe-widget social-widget wow zoomIn animated" data-wow-offset="10" data-wow-delay="0.5s">
                        <h4>Follow Us<span class="head-line"></span></h4>
                        <ul class="social-icons">
                            <li><a class="facebook" href="https://www.facebook.com/Meekla-112837315830999/"><i class="fa fa-facebook"></i></a></li>
                            <li><a class="twitter" href="https://twitter.com/meek_la"><i class="fa fa-twitter"></i></a></li>
                            <li><a class="linkedin" href="https://www.linkedin.com/company/meekla"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
                <%--                    <div class="col-md-3 col-xs-12">
                        <div class="footer-widget twitter-widget wow zoomIn animated" data-wow-offset="10" data-wow-delay="0.5s">
                            <h4>Twitter Feed<span class="head-line"></span></h4>
                            <ul>
                                <li>
                                    <p><a href="#">@Meek.la </a>Where your's physical items are</p>
                                    <span>28 June 2016</span>
                                </li>
                                <li>
                                    <p><a href="#">@Meek.la </a>How to use Beacons for houses #IoT #SmartHouse</p>
                                    <span>22 June 2015</span>
                                </li>
                                <li>
                                    <p><a href="#">@Meek.la </a>Wow! did get service request under one hour from our cust</p>
                                    <span>18 June 2016</span>
                                </li>
                            </ul>
                        </div>
                    </div>--%>
                <div class="col-md-3 col-xs-12">
                    <div class="footer-widget flickr-widget wow zoomIn animated" data-wow-offset="10" data-wow-delay="0.5s">
                        <h4>Our Sites<span class="head-line"></span></h4>
                        <ul>
                            <li><a href="/">Home</a></li>
                            <li><a href="/AboutUs.aspx">About Meekla</a></li>
                            <li><a href="/Privacy.aspx">Privacy policy</a></li>
                            <li><a href="/">Signup!</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-xs-12">
                    <div class="footer-widget contact-widget wow zoomIn animated" data-wow-offset="10" data-wow-delay="0.5s">
                        <h4>Meekla<span class="head-line"></span></h4>
                        <p>The best app for your things! Contact us with any questions, ideas, etc.</p>
                        <ul>
                            <li><span>Email:</span> info@meekla.com</li>
                            <li><span>Website:</span> www.meekla.com</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="copyright-section wow zoomIn animated" data-wow-offset="10" data-wow-delay="0.5s">
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <p>&copy; 2016 Meekla - All Rights Reserved <a href="#">Meekla.com</a></p>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <ul class="footer-nav">
                            <li><a href="#">Sitemap</a></li>
                            <li><a href="/Privacy.aspx">Privacy Policy</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</body>
</html>
