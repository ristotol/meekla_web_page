﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Data.Entity;

namespace Lando.Models
{
    public class Users
    {
        [Key]
        public Guid userID { get; set; }
        //facebook ID
        public string contactfbid { get; set; }
        // email address
        [DataType(DataType.EmailAddress)]
        public string contactemail { get; set; }
        public DateTime datesaved { get; set; }
    }
    public class UsersDBContext : DbContext
    {
        public UsersDBContext ()
            : base("DefaultConnection")
        { }
    }
}